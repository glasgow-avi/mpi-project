#include <mpi.h>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <cstring>

int main(int argc, char* argv[])
{
	int rank, size;
	int i, j, k, m, n, ch, sr, sg, sb;
	int height, width, padding;
	int row;

	int con[3][3];
	static unsigned char header[54];
	static unsigned char r[512][512], g[512][512], b[512][512];
	static unsigned char fr[512][512], fg[512][512], fb[512][512];
	static unsigned char nr[512][512], ng[512][512], nb[512][512];

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	row = 512 / size;

	if (rank == 0) {

		if(argc != 3)
		{
			printf("Usage: mpiexec -n <n> ConsoleApplication1.exe input.bmp output.bmp\n");
			exit(0);
		}

		char filename[20];
		strcpy(filename, argv[1]);
		FILE *fd;

		fd = fopen(filename, "rb");
		if (fd == NULL) {
			printf("Error: fopen failed for %s\n", filename);
			return 0;
		}

		/*	// Read header*/

		fread(header, sizeof(unsigned char), 54, fd);

		width = *(int*)&header[18];
		height = *(int*)&header[22];
		padding = 0;

		while ((width * 3 + padding) % 4 != 0) {
			padding++;
		}

		static unsigned char black[512][512][3];

		for (i = 0, j = 0; i<height; j++)
		{
			fread(black[i][j], sizeof(unsigned char), 3, fd);
			r[i][j] = black[i][j][0];
			g[i][j] = black[i][j][1];
			b[i][j] = black[i][j][2];
			if (j == (width - 1))
			{
				j = -1;
				i++;
			}
		}
		fclose(fd);

	}
	MPI_Bcast(r, 512 * 512, MPI_CHAR, 0, MPI_COMM_WORLD);
	MPI_Bcast(g, 512 * 512, MPI_CHAR, 0, MPI_COMM_WORLD);
	MPI_Bcast(b, 512 * 512, MPI_CHAR, 0, MPI_COMM_WORLD);

	for (i = rank*row; i < (rank + 1)*row; i++) {
		for (j = 0; j < 512; j++) {
			nr[i - (rank*row)][j] = 255 - r[i][j];
			ng[i - (rank*row)][j] = 255 - g[i][j];
			nb[i - (rank*row)][j] = 255 - b[i][j];
		}
	}


	MPI_Gather(nr, row * 512, MPI_CHAR, fr, row * 512, MPI_CHAR, 0, MPI_COMM_WORLD);
	MPI_Gather(ng, row * 512, MPI_CHAR, fg, row * 512, MPI_CHAR, 0, MPI_COMM_WORLD);
	MPI_Gather(nb, row * 512, MPI_CHAR, fb, row * 512, MPI_CHAR, 0, MPI_COMM_WORLD);


	if (rank == 0) {

		unsigned char black1[512][512][3];
		FILE* valdat_op;

		for (i = 0; i < height; i++) {
			for (j = 0; j < width; j++) {
				black1[i][j][0] = fr[i][j];
				black1[i][j][1] = fg[i][j];
				black1[i][j][2] = fb[i][j];
			}
		}

		FILE *fd1;

		fd1 = fopen(argv[2], "wb");
		if (fd1 == NULL)
		{
			printf("Error: fopen failed\n");
			return 0;
		}

		fwrite(header, sizeof(unsigned char), 54, fd1);
		for (i = 0; i<height; i++)
		{
			for (j = 0; j<width; j++)
			{
				fwrite(black1[i][j], sizeof(unsigned char), 3, fd1);
			}
		}

		fclose(fd1);

	}

	MPI_Finalize();
	return 0;
}